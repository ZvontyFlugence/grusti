#[cfg(windows)] extern crate winapi;
use std::io::Error;

// WINAPI types
use winapi::shared::windef::HWND;
use winapi::shared::ntdef::LPCWSTR;
use winapi::shared::minwindef::UINT;

// WINAPI components
use winapi::um::winuser::{
  MB_ABORTRETRYIGNORE,
  MB_CANCELTRYCONTINUE,
  MB_HELP,
  MB_OK,
  MB_OKCANCEL,
  MB_RETRYCANCEL,
  MB_YESNO,
  MB_YESNOCANCEL,
  MessageBoxW,
};

struct MessageBoxBinding {
  hwnd: HWND,
  lp_text: LPCWSTR,
  lp_caption: LPCWSTR,
  u_type: UINT
}

impl MessageBoxBinding {
  pub fn new(hwnd: HWND, lp_text: LPCWSTR, lp_caption: LPCWSTR, u_type: UINT) -> MessageBoxBinding {
    MessageBoxBinding {
      hwnd,
      lp_text,
      lp_caption,
      u_type
    }
  }

  pub fn display(&self) -> Result<i32, Error> {
    let ret = unsafe {
      MessageBoxW(self.hwnd, self.lp_text, self.lp_caption, self.u_type)
    };

    if ret == 0 {
      Err(Error::last_os_error())
    } else {
      Ok(ret)
    }
  }
}

pub enum MB {
  AbortRetryIgnore,
  CancelTryContinue,
  Help,
  OK,
  OKCancel,
  RetryCancel,
  YesNo,
  YesNoCancel
} 

pub struct MessageBox {
  // TODO: Figure out type for parent window
  pub parent_window: HWND,
  pub message: String,
  pub title: String,
  pub behavior: MB,
  binding: Box<MessageBoxBinding>
}

impl MessageBox {
  pub fn new(parent_window: HWND, message: String, title: String, behavior: MB) -> MessageBox {
    // WINAPI functions
    use std::ffi::OsStr;
    use std::iter::once;
    use std::os::windows::ffi::OsStrExt;

    let lp_text: Vec<u16> = OsStr::new(&message).encode_wide().chain(once(0)).collect();
    let lp_caption: Vec<u16> = OsStr::new(&title).encode_wide().chain(once(0)).collect();
    let u_type: UINT = match behavior {
      MB::AbortRetryIgnore => MB_ABORTRETRYIGNORE,
      MB::CancelTryContinue => MB_CANCELTRYCONTINUE,
      MB::Help => MB_HELP,
      MB::OK => MB_OK,
      MB::OKCancel => MB_OKCANCEL,
      MB::RetryCancel => MB_RETRYCANCEL,
      MB::YesNo => MB_YESNO,
      MB::YesNoCancel => MB_YESNOCANCEL,
    };
    let binding = Box::new(
      MessageBoxBinding::new(
        parent_window,
        lp_text.as_ptr(),
        lp_caption.as_ptr(),
        u_type
      )
    );

    MessageBox {
      parent_window,
      message,
      title,
      behavior,
      binding
    }
  }

  pub fn display(&self) -> Result<i32, Error> {
    self.binding.display()
  }
}
