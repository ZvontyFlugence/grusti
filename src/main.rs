#[cfg(windows)] extern crate winapi;
mod lib;

use std::ptr::null_mut;
use crate::lib::{MB, MessageBox};


#[cfg(windows)]
fn main() {
    let mb = MessageBox::new(null_mut(), String::from("Message"), String::from("Title"), MB::OK);

    let ret = mb.display();

    if ret.is_err() {
        println!("An Error Occurred!");
    }
}